package com.intergamma.ProductManagement.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.intergamma.ProductManagement.service.validators.StoreConstraint;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
@StoreConstraint
@Table(name = "STORE")
public class Store {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String storeId;
    private String city;
    private String postalCode;
    private String street;
    private String address_number;
    private String todayOpen;
    private Boolean collectionPoint;
    private String todayClose;
}
