package com.intergamma.ProductManagement.service;

import com.intergamma.ProductManagement.domain.Product;
import com.intergamma.ProductManagement.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<Product> getProductsByStore(String storeId) {
        return productRepository.findByStoreStoreId(storeId);
    }

    @Override
    public List<Product> getProductsByArticle(String articleId) {
        return productRepository.findByArticleArticleId(articleId);
    }

    @Override
    public List<Product> getProductsByArticleAndStore(String articleId, String storeId) {
        return productRepository.findByArticleArticleIdAndStoreStoreId(articleId, storeId);
    }

    @Override
    public Product addProduct(String storeId) {
        return null;
    }

    @Override
    public void deleteProduct(String storeId) {

    }

    @Override
    public Product getProductByProductId(String productId) {
        return productRepository.findByProductId(productId);
    }
}
