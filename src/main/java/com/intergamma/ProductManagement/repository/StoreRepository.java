package com.intergamma.ProductManagement.repository;

import com.intergamma.ProductManagement.domain.Store;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StoreRepository extends JpaRepository<Store, String> {
    List<com.intergamma.ProductManagement.domain.Store> findAll();
}
