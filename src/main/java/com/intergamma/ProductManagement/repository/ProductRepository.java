package com.intergamma.ProductManagement.repository;

import com.intergamma.ProductManagement.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    List<Product> findByStoreStoreId(String storeId);

    List<Product> findByArticleArticleId(String articleId);

    List<Product> findByArticleArticleIdAndStoreStoreId(String articleId, String storeId);

    Product findByProductId(String productId);
}
