package com.intergamma.ProductManagement.controller;


import com.intergamma.ProductManagement.domain.Product;
import com.intergamma.ProductManagement.domain.Store;
import com.intergamma.ProductManagement.service.ProductService;
import com.intergamma.ProductManagement.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
public class StoreController {
    private final StoreService storeService;
    private final ProductService productService;

    @Autowired
    public StoreController(StoreService storeService,
                           ProductService productService) {
        this.storeService = storeService;
        this.productService = productService;
    }

    @GetMapping(path = "/stores", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Store> getStores() {
        return storeService.getStores();
    }

    @GetMapping(path = "/stores/{storeId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Store getStore(@PathVariable String storeId) {
        return storeService.getStore(storeId);
    }

    @PostMapping(path = "/stores", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void addStore(@RequestBody @Valid Store store) {
        storeService.addStore(store);
    }

    @DeleteMapping(path = "/stores/{storeId}")
    public void deleteStore(@PathVariable String storeId) {
        storeService.deleteStore(storeId);
    }

    @GetMapping(path = "/stores/{storeId}/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getProducts(@PathVariable String storeId) {
        return productService.getProductsByStore(storeId);
    }

    @GetMapping(path = "/stores/{storeId}/products/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Product getProduct(@PathVariable String storeId,
                              @PathVariable String productId) {
        return productService.getProductByProductId(productId);
    }

    @GetMapping(path = "/articles/{articleId}/products", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getProductsByArticle(@PathVariable String articleId) {
        return productService.getProductsByArticle(articleId);
    }

    @GetMapping(path = "/products/{productId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Product getProduct(@PathVariable String productId) {
        return productService.getProductByProductId(productId);
    }

    @GetMapping(path = "/stores/{storeId}/products/artifacts/{artifactId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Product> getProductsByArticleAndStore(@PathVariable String artifactId,
                                                      @PathVariable String storeId) {
        return productService.getProductsByArticleAndStore(artifactId, storeId);

    }
}
